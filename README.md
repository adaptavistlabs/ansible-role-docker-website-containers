# Ansible Role: Docker Website Containers
## Public repository

This repository has been made *public* so that it can be easily consumed by Ansible Galaxy.

**THEREFORE PLEASE HEED CAUTION WHEN MAKING ANY CHANGES!**


--

Copyright (C) Adaptavist - All Rights Reserved
Unauthorized copying of this source-code repository, via any medium is strictly prohibited
Proprietary and confidential
Written by Colin Watts <cwatts@adaptavist.com>, November 2019
